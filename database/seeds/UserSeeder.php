<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'full_name' => 'Administrador',
            'username' => 'Admin',
            'email' => str_random(10).'@testmail.com',
            'password' => bcrypt('password'),
        ]);
    }
}
