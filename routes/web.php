<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/*Rutas de authenticate*/
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

/*Rutas del Dashboard*/
Route::get('/', 'AdminController@index')->name('dashboard');
Route::get('dashboard', 'AdminController@index');

/*Rutas de crud*/
Route::post('create', 'AdminController@store')->name('user.create');
Route::get('edit/{id}', 'AdminController@edit')->name('user.edit');
Route::post('edit/{id}', 'AdminController@update');
Route::post('delete/{id}', 'AdminController@delete')->name('user.delete');

/*Ruta del searchbox*/
Route::get('search', 'AdminController@search')->name('search');
Route::get('ajax-pagination','AdminController@pagination')->name('user.pagination');

