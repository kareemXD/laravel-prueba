$(function(){

	var _token = $('meta[name="csrf-token"]').attr("content");
	/*Ocultar alerts despues de 5 segundos*/
	setTimeout(function(){
		$(".alert").slideUp(500);
	}, 5000);

	//método para el searchbox
	$("#searchbox").on("keyup", function(event){
		$this = $(this);
		$.ajax({
			type: "get",
			url: $this.data("routeSearch"),
			data: {searchbox: $this.val(), _token: _token},
			success: function(response){
				if(response.length > 3)
				{
					$("table").parent().html(response);
				}
			}
		})
	});
	/*fin metodo searchbox*/
	/*paginacion*/
    $(document).on('click touchstart', '.pagination a',function(event)
    {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var url =  $(this).attr('href');
        var page = $(this).attr('href').split('page=')[1];
        var route = $("#pagination").data('routePaginate') +'?page='+page;
        var searchbox = null;
        if($("#searchbox").val() != "") {
        	searchbox = $("#searchbox").val();
        }

        $.ajax(
        {
            url: route,
            type: "get",
            data: {searchbox: searchbox},
            success:function(response) {
            	if(response.length > 3)
				{
					$("table").parent().html(response);
				}
            }
        })
    });
    /*Fin paginacion*/
    /*Cargar edit */
    /*crear nuevo usuario*/
    $("#formCreate").on("submit", function(event){
    	event.preventDefault();
    	$this = $(this);

    	if($this.find("input[name='full-name']").val() == "")
    	{
    		showAlertMessage($this, "Nombre obligatorio");
    		return;
    	}


    	if($this.find("input[name='email']").val() == "")
    	{
    		showAlertMessage($this, "Email obligatorio");
    		return;
    	}

    	if(validateEmail($this.find("input[name='email']").val())){
    		showAlertMessage($this, "Ingrese email valido");
    		return;
    	}

    	$.ajax({
    		url: $this.attr("action"),
    		method: "post",
    		data: $this.serializeArray(),
    		success:function(response){
    			if(isJson(response))
    			{
    				if(JSON.parse(response) == false){
    					showAlertMessage($this, "Ups, algo salió mal");

    					return;
    				}
    			}
    			reloadTable();
    			$("#modalCreate").modal("toggle");
    			setTimeout(function(){
    				showAlertMessageSuccess("Creado correctamente");
    			}, 1000);
    		}, 
    		error: function(){
    			showAlertMessage($this, "Ups, algo salió mal");
    		}
    	})
    });
    /*Fin del crear*/

  	/*Cargar info del usuario*/
    $('#modalEdit').on('shown.bs.modal', function (event) {
		$this = $(this);
		$button = $(event.relatedTarget);
		$this.find(".modal-body").load($button.data("route"));
	});
    /*Fin cargar info*/

     /*modificar usuario*/
    $(document).on("submit", "#formUpdate", function(event){
    	event.preventDefault();
    	$this = $(this);

    	if($this.find("input[name='full-name']").val() == "")
    	{
    		showAlertMessage($this, "Nombre obligatorio");
    		return;
    	}


    	if($this.find("input[name='email']").val() == "")
    	{
    		showAlertMessage($this, "Email obligatorio");
    		return;
    	}

    	if(validateEmail($this.find("input[name='email']").val())){
    		showAlertMessage($this, "Ingrese email valido");
    		return;
    	}

    	$.ajax({
    		url: $this.attr("action"),
    		method: "post",
    		data: $this.serializeArray(),
    		success:function(response){
    			if(isJson(response))
    			{
    				if(JSON.parse(response) == false){
    					showAlertMessage($this, "Ups, algo salió mal");

    					return;
    				}
    			}
    			reloadTable();
    			$("#modalEdit").modal("toggle");
    			setTimeout(function(){
    				showAlertMessageSuccess("Modificado correctamente");
    			}, 1000);

    		}, 
    		error: function(){
    			showAlertMessage($this, "Ups, algo salió mal");
    		}
    	})
    });
    /*Fin del modificar usuario*/

    /*modal de confirmacion*/
    $("#modalDelete").on("shown.bs.modal", function(event){
    	$button = $(event.relatedTarget);
    	$("#formDelete").attr("action", $button.data("route"));
    });
    /*fin modal confirmacion*/

    /*Metodo de eliminar*/
    $("#formDelete").on("submit", function(event){
    	$this = $(this);
    	event.preventDefault();
    	$.ajax({
    		url: $this.attr("action"),
    		method: "post",
    		data: $this.serializeArray(),
    		success:function(response){
    			if(isJson(response))
    			{
    				if(JSON.parse(response) == false){
    					showAlertMessage($this, "Ups, algo salió mal");

    					return;
    				}
    			}
    			reloadTable();
    			$("#modalDelete").modal("toggle");
    			setTimeout(function(){
    				showAlertMessageSuccess("Usuario eliminado");
    			}, 1000);
    		}, 
    		error: function(){
    			showAlertMessage($this, "Ups, algo salió mal");
    		}
    	});
    })
    /*fin metodo de eliminar*/


    function showAlertMessage(form, message) {
    	$(".alert").slideUp(100);
    	$(".alert").remove();
    	$(form).prepend('<div class="alert alert-danger">' + message + '</div>');
    }

    function showAlertMessageSuccess(message) {
    	$(".alert").slideUp(100);
    	$(".alert").remove();
    	$("table").parent().prepend('<div class="alert alert-success">' + message + '</div>');
    }

    function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return !re.test(email);
	}

	function isJson(str) {
	    try {
	        JSON.parse(str);
	    } catch (e) {
	        return false;
	    }
	    return true;
	}

	function reloadTable()
	{


        var route = $("#tableUsers").data('routeReload');
        var searchbox = null;
        if($("#searchbox").val() != "") {
        	searchbox = $("#searchbox").val();
        }

        $.ajax(
        {
            url: route,
            type: "get",
            data: {searchbox: searchbox},
            success:function(response) {
            	if(response.length > 3)
				{
					$("table").parent().html(response);
				}
            }
        })

	}
});