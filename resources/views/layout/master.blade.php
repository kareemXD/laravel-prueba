<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

	<title>{{ config('app.name') }} @yield('title')</title>
	{{--Archivos comprimidos--}}
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">

</head>
<body>
	<div id="app">
		@if(Auth::check())
		<nav class="navbar navbar-expand-lg bg-white mb-5">
		 	
		  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
		    <span class="navbar-toggler-icon"></span>
		  </button>
		  <div class="collapse navbar-collapse justify-content-between pl-3 pr-3" id="navbarNav">
			    <a class="navbar-brand text-info" href="#">
					<h4 class="text-info">Bienvenido <small class="text-muted">{{ Auth::user()->full_name }}</small></h4>
				</a>
			
		     <a class="nav-link  h5 text-info border-vertical" href="{{ route('logout') }}">
		      	Log Out 
		    </a>
		  </div>
		</nav>
		@endif
		@yield('content')
		
	</div>
	{{--Comprimido de javascript--}}
	<script src="{{ asset('js/app.js') }}"></script>
</body>
</html>