@extends('layout.master')
@section('title', 'Dashboard')
@section('content')
<div class="container-fluid">
	<div class="row p-5">
		{{--aside--}}
		<div class="col-2 p-0 bg-white">
			<div class="profile-content">
				<img src="http://lorempixel.com/500/500/" class="img-fluid rounded-circle img-profile" alt="">
				<img src="http://lorempixel.com/500/500/" class="img-fluid bg-custom" alt="">
			</div>
			<div class="row mt-0 bg-gray">
				<div class="col p-4">
					<p class="text-muted pl-2 m-0">Backend developer 
						<span class="text-info">
							<i class="fa fa-circle" aria-hidden="true"></i>
							<i class="fa fa-circle" aria-hidden="true"></i>
							<i class="fa fa-circle" aria-hidden="true"></i>
						</span>
					</p>
					<a class="btn btn-link text-info p-0 pl-2" href='mailto: developer@marivalgroup.com'>developer@marivalgroup.com</a>
					
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-10 offset-1">
					<hr>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-10 offset-1">
					<hr>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-10 offset-1">
					<hr>
				</div>
			</div>
			<div class="row mt-5">
				<div class="col-10 offset-1">
					<hr>
				</div>
			</div>
		</div>
		<div class="col-10">
			<div class="row justify-content-center">
				<div class="col-11 bg-white p-0">
					
					<div class="input-group mb-3 input-group-custom ">
					 	<input data-route-search="{{ route('search') }}" name="searchbox" id="searchbox" type="text" class="form-control bg-white rounded-0" placeholder="Lista de usuarios registrados" aria-label="Lista de usuarios registrados" aria-describedby="btn_add">
					 	<div class="input-group-append">
					    <button data-toggle="modal" data-target="#modalCreate" class="btn btn-info rounded-0" type="button" id="btn_add">Nuevo <i class="fa fa-plus" aria-hidden="true"></i></button>
					 	</div>
					</div>

					<div class="pr-4 pl-4">
						@include('partials.users')
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
	{{--Modal create--}}
	<div class="modal fade" id="modalCreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		    	<div class="modal-header">
		        	<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			        	<span aria-hidden="true">&times;</span>
			        </button>
		    	</div>
		    	<div class="modal-body">
		        	@include('partials.user-create')

		    	</div>
		    </div>
	    </div>
	</div>
	{{--Fin modal edit--}}
	{{--Modal edit--}}
	<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-lg" role="document">
		    <div class="modal-content">
		    	<div class="modal-header">
		        	<h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
		        	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
		        	<span aria-hidden="true">&times;</span>
		        </button>
		    	</div>
		    	<div class="modal-body">

		    	</div>
		    </div>
	    </div>
	</div>
	{{--Fin modal edit--}}

	{{--Modal delete--}}
	<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	    <div class="modal-dialog">
	        <div class="modal-content">
	            <div class="modal-header">
	                ¿Está seguro que quiere eliminar este usuario?
	            </div>
	            <div class="modal-body">
	                <form id="formDelete" action="#" method="post">
						{{ csrf_field() }}
						
						<div class="form-group text-right mt-5">
						    <button  data-dismiss="modal" type="button" class="btn btn-link">Cancelar</button>
						    <button  type="submit" class="btn btn-danger rounded-0">Confirmar</button> 
						</div>
					</form>
	            </div>
	            
	        </div>
	    </div>
	</div>
	{{--Fin modal delete--}}
</div>
@endsection