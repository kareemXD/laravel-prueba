<table id="tableUsers" class="table" data-route-reload="{{ route("user.pagination") }}">
	<thead>
		<tr>
			<th>ID</th>
			<th>Nombre:</th>
			<th>Correo</th>
			<th>Opciones</th>
		</tr>
	</thead>
	<tbody>
		@foreach($users as $index => $user)
		<tr>
			<td>{{ $user->id }}</td>
			<td>{{ $user->full_name }}</td>
			<td>{{ $user->email }}</td>
			<td>
				<button data-toggle="modal" data-target="#modalEdit" id="btn_edit" data-route="{{ route('user.edit', $user->id) }}" type="button" class="btn btn-info  btn-rounded">Editar</button>
				<button id="btn_delete" data-route="{{ route('user.delete', $user->id) }}" type="button" class="btn btn-danger  btn-rounded" data-toggle="modal" data-target="#modalDelete">Eliminar</button>
			</td>
		</tr>
		@endforeach
		@if(count($users) == 0)
		<tr>
			<td>Sin datos.</td>
		</tr>
		
		@endif
	</tbody>
</table>
<div id="pagination" class="pagination-content" data-route-paginate="{{ route('user.pagination') }}" >
	{{ $users->render("pagination::bootstrap-4") }}
</div>