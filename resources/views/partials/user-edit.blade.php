<form id="formUpdate" action="{{ route('user.edit', $user->id) }}" method="post">
	{{ csrf_field() }}

	<div class="form-group row">
		<label for="full-name-edit" class="col-sm-2 col-form-label">Nombre Completo</label>
		<div class="col-10">
			<input value="{{ $user->full_name }}" type="text" class="form-control rounded-0 border-0" name="full-name" id="full-name-edit">
		</div>
	</div>
	<div class="form-group row">
		<label for="email-edit" class="col-sm-2 col-form-label">Email</label>
		<div class="col-10">
			<input value="{{ $user->email }}" type="text" class="form-control rounded-0 border-0" name="email" id="email-edit">
		</div>
	</div>
	<div class="form-group text-right mt-5">
	    <button  data-dismiss="modal" type="button" class="btn btn-link">Cancelar</button>
	    <button  type="submit" class="btn btn-info rounded-0">Modificar</button> 
	</div>
</form>