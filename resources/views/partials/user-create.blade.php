<form id="formCreate" action="{{ route('user.create') }}" method="post">
	{{ csrf_field() }}

	<div class="form-group row">
		<label for="full-name-create" class="col-sm-2 col-form-label">Nombre Completo</label>
		<div class="col-10">
			<input type="text" class="form-control rounded-0 border-0" name="full-name" id="full-name-create">
		</div>
	</div>
	<div class="form-group row">
		<label for="email-create" class="col-sm-2 col-form-label">Email</label>
		<div class="col-10">
			<input type="text" class="form-control rounded-0 border-0" name="email" id="email-create">
		</div>
	</div>
	<div class="form-group text-right mt-5">
	    <button  data-dismiss="modal" type="button" class="btn btn-link">Cancelar</button>
	    <button  type="submit" class="btn btn-info rounded-0">Registrar</button> 
	</div>
</form>