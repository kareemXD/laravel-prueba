@extends('layout.master')

@section('title', 'Login')
@section('content')
    <div class="container ">
        <div class="row justify-content-center width-max">
            <div class="col-12 col-md-6 align-self-center">
                <div class="row bg-white">
                    <div class="col-6 p-0">
                        <img src="img/img-view-login.png" alt="Paisaje" title="paisaje" class="img-fluid">
                    </div>
                    <div class="col-6 p-3 pt-4">
                        <h4 class="text-info">BIENVENIDO</h4>
                        <p class="text-muted">Sistema de log-in de Marival Group</p>
                        @if(count($errors) > 0)
                        <div class="alert alert-danger">
                            <strong>{{ $errors->first() }}</strong>
                        </div>
                        @endif
                        <form action="{{ route('login') }}" method="post" class="form-horizontal">
                        	{{ csrf_field() }}

                        	<div class="form-group">
                        		<label for="username">Usuario:</label>
                        		<input name="username" id="username" required="required" type="text" class="form-control rounded-0 border-0">
                        	</div>
                            <div class="form-group">
                                <label for="password">Contraseña:</label>
                                <input name="password" required="required" type="text" class="form-control rounded-0 border-0" id="password">
                            </div>
                            
                            <div class="form-group text-right mt-5">
                                <button  type="button" class="btn btn-link">Cancelar</button>
                                <button  type="submit" class="btn btn-info btn-rounded">Entrar <i class="fa fa-angle-right" aria-hidden="true"></i></button> 
                            </div>
                            
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection