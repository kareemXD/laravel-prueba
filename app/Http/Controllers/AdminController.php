<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\User;


class AdminController extends Controller
{

    private $user;

	public function __construct(User $user)
	{
		$this->middleware('auth');
        $this->user = $user;
	}

    public function index()
    {
        $users = $this->user->paginate(5);
    	return view('dashboard.index', compact('users'));
    }

    /*metodo para crear usuario*/
    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
    		'full-name' => 'required|min:3|max:50|unique:users,full_name',
    		'email' => 'required|email'
    	],[
    		'full-name.required' => 'Nombre obligatorio',
    		'full-name.min' => 'el nombre debe tener mínimo 3 caracteres',
    		'full-name.max' => 'el nombre debe tener máximo 3 caracteres',
    		'full-name.unique' => 'ingrese otro nombre',

    		'email.email' => 'ingrese correo válido',
    		'email.required' => 'Correo obligatorio'
    	]);

    	if($validator->fails())
    	{
    		return json_encode(false);
    	}
        $username = (explode('@', $request->input('email')))[0];

    	$this->user->create([
            'full_name' => $request->input("full-name"),
            "username" => $username,
            "password" => bcrypt(str_random(6)),
            "email" => $request->input("email")
        ]);

        return "success";

    }

    /*metodo para mostrar info del usuario*/
    public function edit($id, Request $request)
    {
        if($request->ajax())
        {
            $user = $this->user->find($id);

            if($user != null)
            {
                return view("partials.user-edit", compact("user"));
            }

            return "Error";
        }

        return redirect("dashboard");

    }

    public function update($id, Request $request)
    {
        if($request->ajax())
        {
            $validator = Validator::make($request->all(), [
                'full-name' => 'required|min:3|max:50|unique:users,full_name,'.$id,
                'email' => 'required|email'
            ],[
                'full-name.required' => 'Nombre obligatorio',
                'full-name.min' => 'el nombre debe tener mínimo 3 caracteres',
                'full-name.max' => 'el nombre debe tener máximo 3 caracteres',
                'full-name.unique' => 'ingrese otro nombre',

                'email.email' => 'ingrese correo válido',
                'email.required' => 'Correo obligatorio'
            ]);
            if($validator->fails())
            {
                return json_encode(false);
            }

            $user = $this->user->find($id);

            $user->email = $request->input("email");
            $user->full_name = $request->input("full-name");
            $user->save();

            return "success";
        }
    }

    public function search(Request $request)
    {

        if($request->ajax())
        {
            $query = $request->input("searchbox");
            $output = "";

            $users = $this->user->where(function($q) use ($query){
                $q->orWhere("email", "like", "%".$query."%")
                ->orWhere("username", "like", "%".$query."%")
                ->orWhere("full_name", "like", "%".$query."%");
            })->paginate(5);

            return view("partials.users", compact("users"));
        }

        return redirect()->route("dashboard");

    }

    /*Metodo para paginar con ajax, tambien se utiliza para actualizar datos de la tabla*/
    public function pagination(Request $request)
    {
        if ($request->ajax()) {
            if($request->input("searchbox") != null)
            {   
                $query = $request->input("searchbox");
                $users = $this->user->where(function($q) use ($query){
                    $q->orWhere("email", "like", "%".$query."%")
                    ->orWhere("username", "like", "%".$query."%")
                    ->orWhere("full_name", "like", "%".$query."%");
                })->paginate(5);
            }else{
                $users = $this->user->paginate(5);
            }
        
            return view('partials.users', compact('users'));
        }    
    }

    /*Metodo para eliminar usuario*/
    public function delete($id, Request $request)
    {
        if($request->ajax())
        {
            $user =$this->user->find($id);
            if($user != null)
            {
                $user->delete();
                return "success";
            }

            return json_encode(false);

        }

        return redirect()->route("dashboard");
    }
    /*fin*/
}
