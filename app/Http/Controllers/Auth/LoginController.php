<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Validator;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = 'dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username(){
        return 'username';
    }

    /*Función que te retorna la vista de login*/
    public function showLoginForm()
    {
        return view('auth.login');
    }

    /*Funcion para iniciar sesion a un usuario*/
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|min:3|max:50',
            'password' => 'required|min:3|max:50'
        ],[
            'username.required' => 'Ingrese nombre de usuario',
            'username.min' => 'El nombre de usuario debe tener al menos 3 caracteres',
            'username.max' => 'El nombre de usuario debe tener maximo 50 caracteres',
            'password.required' => 'Ingrese contraseña de usuario',
            'password.min' => 'La contraseña debe tener al menos 3 caracteres',
            'password.max' => 'La contraseña  debe tener maximo 50 caracteres',
        ]);
        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator);
        }

        $credentials = $request->only("username", "password");

        if (Auth::attempt($credentials)) {
            
            return redirect()->intended('dashboard');
        }

        return redirect()->back()->withErrors("Credenciales incorrectas");

    }
}
